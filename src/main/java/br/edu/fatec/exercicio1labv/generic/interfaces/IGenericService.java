package br.edu.fatec.exercicio1labv.generic.interfaces;

import java.util.List;
import java.util.Optional;

public interface IGenericService<T,P,Id> {

	public T salvar(P param);
	
	public T remover(P param);
	
	public void atualizar(Id param1, P paramUpdate);

	public Optional<T> findById(Long id);
	
	public List<T> findAll();
}
