package br.edu.fatec.exercicio1labv.generic.interfaces;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import br.edu.fatec.exercicio1labv.entities.Cliente;

public interface GenericController<T,P,Id> {
	
	public T salvar(P param,HttpServletRequest request, HttpServletResponse response);
	
	public T remover(P param);
	
	public T atualizar(Id id, @RequestBody @Valid P paramUpdate);

	public ResponseEntity<T> getById(Long id);
	
	public ResponseEntity<Collection<T>> getAll();

	public List<T> salvarAll(@Valid List<T> listEntity, HttpServletRequest request, HttpServletResponse response);

}
