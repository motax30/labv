package br.edu.fatec.exercicio1labv.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.fatec.exercicio1labv.entities.Carro;
import br.edu.fatec.exercicio1labv.entities.Cliente;
import br.edu.fatec.exercicio1labv.repositories.ICarroRepository;
import br.edu.fatec.exercicio1labv.repositories.IClienteRepository;
import br.edu.fatec.exercicio1labv.repositories.IEnderecoRepository;
import br.edu.fatec.exercicio1labv.services.IClienteService;

@Service
public class ClienteServiceImpl implements IClienteService {

	@Autowired
	public IClienteRepository clienteRepository;
	
	@Autowired
	public IEnderecoRepository enderecoRepository;
	
	@Autowired
	public ICarroRepository carroRepository;
	
	@Override
	public Cliente salvar(Cliente cliente) {
		if(cliente.getCarros()!=null && !cliente.getCarros().isEmpty()) {
			for(Carro carro: cliente.getCarros()) {
				carro.setCliente(cliente);
			}
		}
		return clienteRepository.save(cliente);
	}


	@Override
	public Cliente remover(Cliente cliente) {
		clienteRepository.delete(cliente);
		return cliente;
	}

	@Override
	public void atualizar(Long id, Cliente clienteUpdate) {
		Optional<Cliente> cliente = clienteRepository.findById(id);
		if(cliente.isPresent()) {
			clienteUpdate.setId(cliente.get().getId());
		}
		salvar(clienteUpdate);
	}

	@Override
	public Optional<Cliente> findById(Long id) {
		return clienteRepository.findById(id);
	}


	@Override
	public List<Cliente> findAll() {
		return clienteRepository.findAll();
	}

	@Override
	public List<Optional<Cliente>> recuperarPorNomeESobreNome(String nome, String sobrenome) {
		return clienteRepository.findByNomeContainingAndSobreNomeContaining(nome,sobrenome);
	}
}