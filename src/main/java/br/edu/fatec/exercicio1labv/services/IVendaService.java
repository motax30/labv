package br.edu.fatec.exercicio1labv.services;

import br.edu.fatec.exercicio1labv.entities.Venda;
import br.edu.fatec.exercicio1labv.generic.interfaces.IGenericService;

public interface IVendaService extends IGenericService<Venda, Venda, Long>{}
