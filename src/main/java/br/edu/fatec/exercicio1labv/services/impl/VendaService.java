package br.edu.fatec.exercicio1labv.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.fatec.exercicio1labv.entities.Venda;
import br.edu.fatec.exercicio1labv.repositories.IVendaRepository;
import br.edu.fatec.exercicio1labv.services.IClienteService;
import br.edu.fatec.exercicio1labv.services.IVendaService;

@Service
public class VendaService implements IVendaService {

	@Autowired
	IVendaRepository vendaRepository;
	
	@Autowired
	IClienteService clienteService;
	
	@Override
	public Venda salvar(Venda venda) {
		if (venda.getCliente() != null) clienteService.salvar(venda.getCliente());
		return vendaRepository.save(venda);
	}

	@Override
	public Venda remover(Venda venda) {
		vendaRepository.delete(venda);
		return venda;
	}

	@Override
	public void atualizar(Long id, Venda vendaUpdate) {
		Optional<Venda> venda = vendaRepository.findById(id);
		if(venda.isPresent()) {
			vendaUpdate.setId(venda.get().getId());
		}
		salvar(vendaUpdate);
	}

	@Override
	public Optional<Venda> findById(Long id) {
		return vendaRepository.findById(id);
	}

	@Override
	public List<Venda> findAll() {
		return vendaRepository.findAll();
	}
}
