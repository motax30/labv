package br.edu.fatec.exercicio1labv.services;

import java.util.List;
import java.util.Optional;

import br.edu.fatec.exercicio1labv.entities.Cliente;
import br.edu.fatec.exercicio1labv.generic.interfaces.IGenericService;


public interface IClienteService extends IGenericService<Cliente, Cliente, Long>{

	List<Optional<Cliente>> recuperarPorNomeESobreNome(String nome, String sobrenome);}
