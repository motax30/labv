package br.edu.fatec.exercicio1labv.services;

import br.edu.fatec.exercicio1labv.entities.Vendedor;
import br.edu.fatec.exercicio1labv.generic.interfaces.IGenericService;

public interface IVendedorService extends IGenericService<Vendedor, Vendedor, Long> {}
