package br.edu.fatec.exercicio1labv.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.fatec.exercicio1labv.entities.Vendedor;
import br.edu.fatec.exercicio1labv.repositories.IVendedorRepository;
import br.edu.fatec.exercicio1labv.services.IVendedorService;

@Service
public class VendedorServiceImpl implements IVendedorService {

	@Autowired
	private IVendedorRepository vendedorRepository;
	
	@Override
	public Vendedor salvar(Vendedor vendedor) {
		return vendedorRepository.save(vendedor);
	}

	@Override
	public Vendedor remover(Vendedor vendedor) {
		vendedorRepository.delete(vendedor);
		return vendedor ;
	}

	@Override
	public void atualizar(Long id, Vendedor vendedorUpdate) {
		Optional<Vendedor> vendedor = findById(id);
		if(vendedor.isPresent()) {
			vendedorUpdate.setId(vendedor.get().getId());
		}
		vendedorRepository.save(vendedorUpdate);
	}

	@Override
	public Optional<Vendedor> findById(Long id) {
		return vendedorRepository.findById(id);
	}

	@Override
	public List<Vendedor> findAll() {
		return vendedorRepository.findAll();
	}

}
