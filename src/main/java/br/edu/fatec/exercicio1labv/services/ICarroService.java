package br.edu.fatec.exercicio1labv.services;

import br.edu.fatec.exercicio1labv.entities.Carro;
import br.edu.fatec.exercicio1labv.generic.interfaces.IGenericService;

public interface ICarroService extends IGenericService<Carro, Carro, Long>{}
