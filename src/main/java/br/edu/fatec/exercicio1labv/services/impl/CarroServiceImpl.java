package br.edu.fatec.exercicio1labv.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.fatec.exercicio1labv.entities.Carro;
import br.edu.fatec.exercicio1labv.repositories.ICarroRepository;
import br.edu.fatec.exercicio1labv.services.ICarroService;

@Service
public class CarroServiceImpl implements ICarroService {

	@Autowired
	private ICarroRepository carroRepository;
	
	@Override
	public Carro salvar(Carro carro) {
		return carroRepository.save(carro);
	}

	@Override
	public Carro remover(Carro carro) {
		carroRepository.delete(carro);
		return carro;
	}

	@Override
	public void atualizar(Long id, Carro carroUpdate) {
		Optional<Carro> carro = carroRepository.findById(id);
		if(carro.isPresent()) {
			carroUpdate.setId(carro.get().getId());
		}
		salvar(carroUpdate);
	}

	@Override
	public Optional<Carro> findById(Long id) {
		return carroRepository.findById(id);
	}

	@Override
	public List<Carro> findAll() {
		return carroRepository.findAll();
	}

}
