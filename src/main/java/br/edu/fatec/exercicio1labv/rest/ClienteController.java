package br.edu.fatec.exercicio1labv.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import br.edu.fatec.exercicio1labv.entities.Cliente;
import br.edu.fatec.exercicio1labv.generic.interfaces.GenericController;
import br.edu.fatec.exercicio1labv.services.impl.ClienteServiceImpl;
import br.gov.sp.fatec.view.View;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteController implements GenericController<Cliente, Cliente, Long>{
	
	@Autowired
	private ClienteServiceImpl clienteService;
	
	@PostMapping("/salvar")
	@JsonView(View.ClienteCompleto.class)
	@Override
	public Cliente salvar(@RequestBody Cliente cliente, HttpServletRequest request, HttpServletResponse response) {
		try {
			cliente = clienteService.salvar(cliente);
			response.addHeader("Location", request.getServerName() +
					":" + request.getServerPort() + request.getContextPath()+
					"/carro/getById?id="+ cliente.getId());
		}
		catch(TransactionSystemException e) {
			response.addHeader("Resposta: ", "O cliente não foi salvo pois seu CPF está nulo");
			return null;
		}
		return cliente;
	}
	
	@PostMapping("/salvar/All")
	@JsonView(View.ClienteCompleto.class)
	@Override
	public List<Cliente> salvarAll(@RequestBody @Valid List<Cliente> clientes, HttpServletRequest request, HttpServletResponse response) {
		try {
			for(Cliente cliente:clientes) {
				cliente = clienteService.salvar(cliente);
				response.addHeader("Location", request.getServerName() +
						":" + request.getServerPort() + request.getContextPath()+
						"/carro/getById?id="+ cliente.getId());
			}
		} catch (Exception e) {
			response.addHeader("Erro: ", HttpStatus.BAD_REQUEST.toString()+ "Não foi possível retornar os clientes.");
		}
		return clientes;
	}

	@DeleteMapping()
	@JsonView(View.ClienteCompleto.class)
	@Override
	public Cliente remover(Cliente cliente) {
		clienteService.remover(cliente);
		return cliente;
	}

	@PutMapping("{id}")
	@JsonView(View.ClienteCompleto.class)
	@Override
	public Cliente atualizar(Long id, @Valid Cliente cliente) {
		clienteService.atualizar(id,cliente);
		return cliente;
	}
	
	@RequestMapping(value = "/getByNomeESobreNome")
	@JsonView(View.ClienteCompleto.class)
	public ResponseEntity<List<Cliente>> recuperarPorNomeESobreNome(@RequestParam(value = "nome") String nome, 
											@RequestParam(value = "sobrenome") String sobrenome) {
		List<Optional<Cliente>> clientes = clienteService.recuperarPorNomeESobreNome(nome, sobrenome);
		if(!clientes.isEmpty()) {
			List<Cliente>lista = new ArrayList<>();
			for (Optional<Cliente> c : clientes) {
				lista.add(c.get());
			}
			return new ResponseEntity<List<Cliente>>(lista,HttpStatus.OK);
		}
		return new ResponseEntity <List<Cliente>>(HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(value = "/getById")
	@JsonView(View.ClienteResumo.class)
	@Override
	public ResponseEntity<Cliente> getById(@RequestParam(value = "id", defaultValue = "1") Long id) {
		Optional<Cliente> cliente = clienteService.findById(id);
		if(!cliente.isPresent()) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Cliente>(cliente.get(), HttpStatus.OK);
	}

	@RequestMapping(value = "/getAll")
	@JsonView(View.ClienteCompleto.class)
	@Override
	public ResponseEntity<Collection<Cliente>> getAll() {
		List<Cliente>clientes = clienteService.findAll();
		if(!clientes.isEmpty()) {
			return new ResponseEntity<Collection<Cliente>>(clientes, HttpStatus.OK);
		}
		return new ResponseEntity<Collection<Cliente>>(HttpStatus.BAD_REQUEST);
	}	
}