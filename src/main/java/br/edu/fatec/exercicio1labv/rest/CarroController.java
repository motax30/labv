package br.edu.fatec.exercicio1labv.rest;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import br.edu.fatec.exercicio1labv.entities.Carro;
import br.edu.fatec.exercicio1labv.entities.Cliente;
import br.edu.fatec.exercicio1labv.generic.interfaces.GenericController;
import br.edu.fatec.exercicio1labv.services.impl.CarroServiceImpl;
import br.gov.sp.fatec.view.View;

@RestController
@RequestMapping(value = "/carro")
public class CarroController implements GenericController<Carro, Carro, Long> {
	
	@Autowired
	private CarroServiceImpl carroService;
	
	@RequestMapping(value = "/getById")
	@JsonView(View.CarroCompleto.class)
	@Override
	public ResponseEntity<Carro> getById(@RequestParam(value = "id", defaultValue = "1") Long id){
		Optional<Carro> carro = carroService.findById(id);
		if(!carro.isPresent()) {
			return new ResponseEntity<Carro>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Carro>(carro.get(), HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getAll")
	@JsonView(View.CarroCompleto.class)
	@Override
	public ResponseEntity<Collection<Carro>> getAll(){
		return new ResponseEntity<Collection<Carro>>(carroService.findAll(), HttpStatus.OK);
	}
	
	
	@PostMapping("/salvar")
	@JsonView(View.CarroCompleto.class)
	@Override
	public Carro salvar(@RequestBody @Valid Carro carro, HttpServletRequest request, HttpServletResponse response) {
		carro = carroService.salvar(carro);
		response.addHeader("Location", request.getServerName() +
				":" + request.getServerPort() + request.getContextPath()+
				"/carro/getById?id="+ carro.getId());
		return carro;
	}
	
	@DeleteMapping()
	@JsonView(View.CarroCompleto.class)
	@Override
	public Carro remover(@RequestBody Carro carro) {
		carroService.remover(carro);
		return carro;
	}
	
	@PutMapping("{id}")
	@JsonView(View.CarroCompleto.class)
	@Override
	public Carro atualizar(@PathVariable(value = "id") Long id, @RequestBody @Valid Carro carro) {
		carroService.atualizar(id, carro);
		return carro;
	}


	@PostMapping("/salvar/All")
	@JsonView(View.ClienteCompleto.class)
	@Override
	public List<Carro> salvarAll(@RequestBody @Valid List<Carro> carros, HttpServletRequest request, HttpServletResponse response) {
		try {
			for(Carro carro:carros) {
				carro = carroService.salvar(carro);
				response.addHeader("Location", request.getServerName() +
						":" + request.getServerPort() + request.getContextPath()+
						"/carro/getById?id="+ carro.getId());
			}
		} catch (Exception e) {
			response.addHeader("Erro: ", HttpStatus.BAD_REQUEST.toString());
		}
		return carros;
	}
}
