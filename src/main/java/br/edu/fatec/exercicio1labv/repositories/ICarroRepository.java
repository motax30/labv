package br.edu.fatec.exercicio1labv.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.edu.fatec.exercicio1labv.entities.Carro;

@Repository
public interface ICarroRepository extends JpaRepository<Carro, Long> {
	
	Optional<List<Carro>> findByMarca(String marca);
	
	@Query("select c from Carro c where c.nome like %?1%")
	List<Optional<Carro>> buscarPorNome(String nome);

}
