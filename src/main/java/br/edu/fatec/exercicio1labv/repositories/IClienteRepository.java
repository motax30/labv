package br.edu.fatec.exercicio1labv.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.edu.fatec.exercicio1labv.entities.Carro;
import br.edu.fatec.exercicio1labv.entities.Cliente;

@Repository
public interface IClienteRepository extends JpaRepository<Cliente, Long> {
	
	Optional<Cliente> findByCpf(Long cpf);
	
	@Query("select c from Cliente c where c.nome like %?1%")
	List<Optional<Cliente>> buscarPorNome(String nome);

	List<Optional<Cliente>> findByNomeContainingAndSobreNomeContaining(String nome, String sobrenome);
	
	
}