package br.edu.fatec.exercicio1labv.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.fatec.exercicio1labv.entities.Endereco;

@Repository
public interface IEnderecoRepository extends JpaRepository<Endereco, Long> {

}
