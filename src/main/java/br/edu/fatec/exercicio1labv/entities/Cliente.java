package br.edu.fatec.exercicio1labv.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonView;

import br.gov.sp.fatec.view.View;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Cliente {
	public void setId(Long id) {
		this.id = id;
	}

	@Id
	@SequenceGenerator(name = "cliente_seq", sequenceName = "cliente_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="cliente_seq")
	@JsonView({View.ClienteCompleto.class})
	private Long id;
	
	@XmlElement(name="nome")
	@JsonView({View.ClienteCompleto.class})
	@Column(name = "cli_nome")
	private String nome;

	@XmlElement(name="nome")
	@JsonView({View.ClienteCompleto.class})
	@Column(name = "cli_sobrenome")
	private String sobreNome;
	
	@JsonView({View.ClienteCompleto.class})
	@XmlElement(name="cpf")
	@Column(name = "cli_cpf")
	private Long cpf;
	
	@JsonView({View.ClienteCompleto.class})
	@XmlElement(name="endereco")
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "end_endereco_id" )
	private Endereco endereco;
	
	@JsonView({View.ClienteCompleto.class})
	@XmlElement(name="carros")
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
	private List<Carro> carros;
	
	public Cliente() {}
	
	private Cliente(String nome, Long cpf, Endereco endereco, List<Carro> carros) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.endereco = endereco;
		this.carros = carros;
	}
	
	public static class Builder{
		private String nome;
		private Long cpf;
		private Endereco endereco;
		private List<Carro> carros;
		
		public Builder() {}
		
		public Builder nome(String nome) {
			this.nome = nome;
			return this;
		}
		
		public Builder cpf(Long cpf) {
			this.cpf = cpf;
			return this;
		}
		
		public Builder endereco(Endereco endereco) {
			this.endereco = endereco;
			return this;
		}
		
		public Builder carros(List<Carro> carros) {
			this.carros = carros;
			return this;
		}
		
		public Cliente build() {
			return new Cliente(nome, cpf, endereco, carros);
		}
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public Long getCpf() {
		return cpf;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public List<Carro> getCarros() {
		return carros;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public void setCarros(List<Carro> carros) {
		this.carros = carros;
	}

	public void alterarCliente(Cliente clienteUpdate) {
		setNome(clienteUpdate.getNome());
		setCpf(clienteUpdate.getCpf());
		getEndereco().alterarEndereco(clienteUpdate.getEndereco());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
