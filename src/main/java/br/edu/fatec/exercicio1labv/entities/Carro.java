package br.edu.fatec.exercicio1labv.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

import br.gov.sp.fatec.view.View;

@Entity
public class Carro {
	
	@Id
	@SequenceGenerator(name = "carro_seq", sequenceName = "carro_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="carro_seq")
	@JsonView({View.CarroCompleto.class, View.CarroResumo.class, View.CarroResumoAlternativo.class})
	private Long id;
	
	@Column(name = "car_nome")
	@JsonView({View.ClienteCompleto.class,View.CarroCompleto.class, View.CarroResumo.class,View.CarroResumoAlternativo.class})
	private String nome;
	
	@Column(name = "car_marca")
	@JsonView({View.ClienteCompleto.class,View.CarroCompleto.class, View.CarroResumo.class,View.CarroResumoAlternativo.class})
	private String marca;
	
	@Column(name = "car_modelo")
	@JsonView({View.ClienteCompleto.class,View.CarroCompleto.class, View.CarroResumo.class,View.CarroResumoAlternativo.class})
	private String modelo;
	
	@Column(name = "car_cor")
	@JsonView({View.CarroCompleto.class, View.CarroResumo.class})
	private String cor;
	
	@ManyToOne
	@JoinColumn(name = "cli_cliente_id")
	@JsonView({View.CarroCompleto.class})
	private Cliente cliente;
	
	@ManyToOne
	@JoinColumn(name = "vnd_venda_id")
	@JsonView({View.CarroCompleto.class})
	private Venda venda;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public String getMarca() {
		return marca;
	}

	public String getModelo() {
		return modelo;
	}

	public String getCor() {
		return cor;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public Venda getVenda() {
		return venda;
	}

	public Carro() {}

	private Carro(String nome, String marca, String modelo, String cor, Cliente cliente, Venda venda) {
		this.nome = nome;
		this.marca = marca;
		this.modelo = modelo;
		this.cor = cor;
		this.cliente = cliente;
		this.venda = venda;
	}
	
	public static class CarroBuilder{
		
		private String nome;
		private String marca;
		private String modelo;
		private String cor;
		private Cliente cliente;
		private Venda venda;
		
		public CarroBuilder() {}

		public CarroBuilder setNome(String nome) {
			this.nome = nome;
			return this;
		}

		public CarroBuilder setMarca(String marca) {
			this.marca = marca;
			return this;
		}

		public CarroBuilder setModelo(String modelo) {
			this.modelo = modelo;
			return this;
		}

		public CarroBuilder setCor(String cor) {
			this.cor = cor;
			return this;
		}

		public CarroBuilder setCliente(Cliente cliente) {
			this.cliente = cliente;
			return this;
		}

		public CarroBuilder setVenda(Venda venda) {
			this.venda = venda;
			return this;
		}
		
		public Carro build() {
			return new Carro(nome, marca, modelo, cor, cliente,venda);
		}
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
