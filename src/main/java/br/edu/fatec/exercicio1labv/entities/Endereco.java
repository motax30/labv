package br.edu.fatec.exercicio1labv.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonView;

import br.gov.sp.fatec.view.View;

@Entity
public class Endereco {
	
	@Id
	@SequenceGenerator(name = "endereco_seq", sequenceName = "endereco_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="endereco_seq")
	private Long id;
	
	@JsonView({View.ClienteCompleto.class})
	@Column(name = "end_rua")
	private String rua;
	
	@JsonView({View.ClienteCompleto.class})
	@Column(name = "end_numero")
	private String numero; 
	
	@JsonView({View.ClienteCompleto.class})
	@Column(name = "end_complemento")
	private String complemento;
	
	@JsonView({View.ClienteCompleto.class})
	@Column(name = "end_bairro")
	private String bairro;
	
	@JsonView({View.ClienteCompleto.class})
	@Column(name = "end_cidade")
	private String cidade;
	
	@JsonView({View.ClienteCompleto.class})
	@Column(name = "end_estado")
	private Estado estado;
	
	public Endereco() {}
	
	public Endereco(String rua, String numero, String complemento, String bairro, String cidade, Estado estado) {
		super();
		this.rua = rua;
		this.numero = numero;
		this.complemento = complemento;
		this.bairro = bairro;
		this.cidade = cidade;
		this.estado = estado;
	}

	public static class EnderecoBuilder{
		private String rua;
		private String numero; 
		private String complemento;
		private String bairro;
		private String cidade;
		private Estado estado;
		
		public EnderecoBuilder() {	
		}
		
		public EnderecoBuilder rua(String rua) {
			this.rua = rua;
			return this;
		}
		
		public EnderecoBuilder numero(String numero) {
			this.numero = numero;
			return this;
		}
		
		public EnderecoBuilder complemento(String complemento) {
			this.complemento = complemento;
			return this;
		}
		
		public EnderecoBuilder bairro(String bairro) {
			this.bairro = bairro;
			return this;
		}
		
		public EnderecoBuilder cidade(String cidade) {
			this.cidade = cidade;
			return this;
		}
		
		public Endereco build(){
			return new Endereco(rua, numero, complemento, bairro, cidade, estado);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public void alterarEndereco(Endereco enderecoUpdate) {
		setRua(enderecoUpdate.getRua());
		setBairro(enderecoUpdate.getBairro());
		setCidade(enderecoUpdate.getCidade());
		setComplemento(enderecoUpdate.getComplemento());
		setEstado(enderecoUpdate.getEstado());
		setNumero(enderecoUpdate.getNumero());
	}
}
