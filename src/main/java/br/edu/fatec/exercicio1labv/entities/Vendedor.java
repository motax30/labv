package br.edu.fatec.exercicio1labv.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Vendedor {
	
	public void setId(Long id) {
		this.id = id;
	}

	@Id
	@SequenceGenerator(name = "vendedor_seq", sequenceName = "vendedor_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="vendedor_seq")
	private Long id;
	
	@Column(name = "vdr_nome")
	private String nome;
	
	@OneToMany(mappedBy = "vendedor")
	private List<Venda> vendas;
	
	public Vendedor() {}
	
	public Vendedor(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setVendas(List<Venda> vendas) {
		this.vendas = vendas;
	}

	public List<Venda> getVendas() {
		return vendas;
	}	
}
