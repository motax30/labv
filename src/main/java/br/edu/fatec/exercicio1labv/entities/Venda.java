package br.edu.fatec.exercicio1labv.entities;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity

public class Venda {
	
	@Id
	@SequenceGenerator(name = "venda_seq", sequenceName = "venda_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="venda_seq")
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "cli_cliente_id")
	private Cliente cliente;
	
	@OneToMany(mappedBy = "venda", fetch = FetchType.EAGER, orphanRemoval = true)
	private List<Carro> carro;
	
	@Column(name = "vnd_data_venda")
	@Temporal(TemporalType.DATE)
	private Calendar data_venda;
	
	@Column(name = "vnd_preco")
	private Double preco;
	
	@ManyToOne
	@JoinColumn(name = "vdr_vendedor_id")
	private Vendedor vendedor;
	
	public Venda() {}
	
	public Venda(Cliente cliente, List<Carro> carro, Calendar data_venda, Double preco, Vendedor vendedor) {
		super();
		this.cliente = cliente;
		this.carro = carro;
		this.data_venda = data_venda;
		this.preco = preco;
		this.vendedor = vendedor;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Carro> getCarro() {
		return carro;
	}

	public void setCarro(List<Carro> carro) {
		this.carro = carro;
	}

	public Calendar getData_venda() {
		return data_venda;
	}

	public void setData_venda(Calendar data_venda) {
		this.data_venda = data_venda;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}
}
