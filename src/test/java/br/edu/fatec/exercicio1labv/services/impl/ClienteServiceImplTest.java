package br.edu.fatec.exercicio1labv.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.edu.fatec.exercicio1labv.entities.Carro;
import br.edu.fatec.exercicio1labv.entities.Cliente;
import br.edu.fatec.exercicio1labv.entities.Endereco;
import br.edu.fatec.exercicio1labv.services.IClienteService;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class ClienteServiceImplTest {

	private Cliente cliente;
	private List<Carro> carros;
	Carro carro1;
	Carro carro2;
	Carro carro3;
	Carro carro4;
	
	@Autowired
	private IClienteService clienteService;
		
	private Endereco endereco;
	private Cliente clienteUpdate;
	private Endereco enderecoUpdate;
	
	@Before
	public void setUp() {
		carro1 = new Carro.CarroBuilder().setNome("Onix").build();
		carro2 = new Carro.CarroBuilder().setNome("Fusca").setMarca("Chevrolet").setCor("Amarelo").build();
		carro3 = 	new Carro.CarroBuilder().setNome("Fit").setMarca("Honda").setCor("Cinza").build();
		carro4 = new Carro.CarroBuilder().setNome("Etios").setMarca("Toiota").setCor("Azul Metálico").build();
		endereco = new Endereco.EnderecoBuilder().rua("Otavio de Moraes")
				.numero("298").bairro("Jardim Americano").cidade("São José dps Campos").build();
		enderecoUpdate = new Endereco.EnderecoBuilder().rua("Carlos de Azevedo")
				.numero("885").bairro("Jardim Aquárius").cidade("São Paulo").build();
		cliente = new Cliente.Builder()
				.nome("Adriano Mota")
				.endereco(endereco)
				.cpf(31083818813L)
				.build();
		clienteUpdate = new Cliente.Builder()
				.nome("Jorge de Carvalho")
				.cpf(25478578596L)
				.endereco(enderecoUpdate)
				.build();
		carro1.setCliente(cliente);
		carro2.setCliente(cliente);
		cliente.setCarros(Arrays.asList(carro1,carro2));
	}
	
	@Test
	public void testSalvarClienteComSucesso() {	
		clienteService.salvar(cliente);
		assertNotNull(clienteService.findById(cliente.getId()));
	}

	@Test
	public void testAtualizar() {
		Cliente c1 = clienteService.salvar(cliente);
		clienteService.atualizar(c1.getId(), clienteUpdate);
		assertNotEquals(c1.getNome(), clienteUpdate.getNome());
		assertNotEquals(c1.getCpf(), clienteUpdate.getCpf());
		assertNotEquals(c1.getEndereco(),  clienteUpdate.getEndereco());
		assertNotEquals(c1.getCarros(), clienteUpdate.getCarros());
	}
	
	@Test
	public void testRemoverClienteSucesso() {
		Cliente c1 = clienteService.salvar(cliente);
		clienteService.remover(c1);
		Optional<Cliente> cliente = clienteService.findById(c1.getId());
		assertNotSame(c1, cliente);
	}
}