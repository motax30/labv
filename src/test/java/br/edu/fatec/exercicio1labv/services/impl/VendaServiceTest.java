package br.edu.fatec.exercicio1labv.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.edu.fatec.exercicio1labv.entities.Carro;
import br.edu.fatec.exercicio1labv.entities.Cliente;
import br.edu.fatec.exercicio1labv.entities.Venda;
import br.edu.fatec.exercicio1labv.entities.Vendedor;
import br.edu.fatec.exercicio1labv.services.ICarroService;
import br.edu.fatec.exercicio1labv.services.IClienteService;
import br.edu.fatec.exercicio1labv.services.IVendaService;
import br.edu.fatec.exercicio1labv.services.IVendedorService;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class VendaServiceTest {

	@Autowired
	private IVendaService vendaService;
	
	@Autowired
	private ICarroService carroService;
	
	@Autowired
	private IClienteService clienteService;
	
	@Autowired
	private IVendedorService vendedorService;
	
	private Cliente cliente;
	private List<Carro> carros;
	private Vendedor vendedor;
	private Venda vendaUpdate;

	private Cliente clienteUpdate;

	private Vendedor vendedorUpdate;

	private List<Carro> carros2;
	
	
	@Before
	public void setUp() throws Exception {
		Carro carro1 = new Carro.CarroBuilder().setNome("Fusca").setMarca("Chevrolet").setCor("Amarelo").build();
		Carro carro2 = new Carro.CarroBuilder().setNome("Fit").setMarca("Honda").setCor("Cinza").build();
		carroService.salvar(carro1);
		carroService.salvar(carro2);

		carros = carroService.findAll();
		
		carros2 = Arrays.asList(carroService.salvar(new Carro.CarroBuilder().setNome("Sandero").setMarca("Renault").setCor("Vermelho").build()),
				carroService.salvar(new Carro.CarroBuilder().setNome("BMW").setMarca("Mitsubish").setCor("Branco").build()));
		
		cliente = new Cliente.Builder()
				.nome("Adriano Mota")
				.cpf(31083818813L)
				.build();
		clienteUpdate = new Cliente.Builder()
				.nome("Fernando Santos")
				.cpf(888111888822L)
				.build();
		clienteService.salvar(cliente);
		vendedor = new Vendedor("Aurélio");
		vendedorService.salvar(vendedor);
		
		vendedorUpdate = new Vendedor("Aurélio");
		vendedorService.salvar(vendedorUpdate);
		
		vendaUpdate = new Venda(clienteUpdate, carros2,Calendar.getInstance(), 40000D, vendedorUpdate);		
	}

	@Test
	public void testSalvar() {
		Venda venda = new Venda(cliente, carros,Calendar.getInstance(), 30000D, vendedor);
		vendaService.salvar(venda);
		assertNotNull(vendaService.findById(venda.getId()));
	}

	@Test
	public void testRemover() {
		Venda venda = new Venda(cliente, carros,Calendar.getInstance(), 30000D, vendedor);
		vendaService.salvar(venda);
		vendaService.remover(venda);
		assertFalse(vendaService.findById(venda.getId()).isPresent());
	}

	@Test
	public void testAtualizar() {
		Venda venda = new Venda(cliente, carros,Calendar.getInstance(), 30000D, vendedor);
		vendaService.salvar(venda);
		vendaService.atualizar(venda.getId(), vendaUpdate);
		assertEquals(venda.getCliente(), vendaUpdate.getCliente());
		assertEquals(venda.getData_venda(),vendaUpdate.getData_venda());
		assertEquals(venda.getPreco(), vendaUpdate.getPreco());
		assertEquals(venda.getVendedor(), vendaUpdate.getVendedor());
	}

	@Test
	public void testFindById() {
		Venda venda = new Venda(cliente, carros,Calendar.getInstance(), 30000D, vendedor);
		vendaService.salvar(venda);
		assertTrue(vendaService.findById(venda.getId()).isPresent());
	}

	@Test
	public void testFindAll() {
		Venda venda = new Venda(cliente, carros,Calendar.getInstance(), 30000D, vendedor);
		vendaService.salvar(venda);
		Venda venda2 = new Venda(cliente, carros,Calendar.getInstance(), 30000D, vendedor);
		vendaService.salvar(venda2);
		assertEquals(2,vendaService.findAll().size());
	}

}
