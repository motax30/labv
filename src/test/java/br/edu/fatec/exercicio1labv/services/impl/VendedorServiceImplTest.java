package br.edu.fatec.exercicio1labv.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.edu.fatec.exercicio1labv.entities.Vendedor;
import br.edu.fatec.exercicio1labv.services.IVendedorService;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class VendedorServiceImplTest {

	@Autowired
	private IVendedorService vendedorService;
	private Vendedor vendedorUpdate;

	@Before
	public void setUp() throws Exception {
		vendedorUpdate = new Vendedor("Nome Alterado");
	}

	@Test
	public void testSalvar() {
		Vendedor vendedor = new Vendedor("Aurélio");
		vendedorService.salvar(vendedor);
		assertTrue(vendedorService.findById(vendedor.getId()).isPresent());
	}

	@Test
	public void testRemover() {
		Vendedor vendedor = new Vendedor("Aurélio");
		Vendedor vendedorSalvo = vendedorService.salvar(vendedor);
		vendedorService.remover(vendedorSalvo);
		assertFalse(vendedorService.findById(vendedor.getId()).isPresent());
	}

	@Test
	public void testAtualizar() {
		Vendedor vendedor = new Vendedor("Aurélio");
		Vendedor vendedorSalvo = vendedorService.salvar(vendedor);
		vendedorService.atualizar(vendedorSalvo.getId(), vendedorUpdate);
		assertEquals(vendedorSalvo.getId(), vendedorUpdate.getId());
		assertEquals(vendedor.getNome(), vendedorUpdate.getNome());
	}

	@Test
	public void testFindById() {
		Vendedor vendedor = new Vendedor("Aurélio");
		Vendedor vendedorSalvo = vendedorService.salvar(vendedor);
		assertTrue(vendedorService.findById(vendedorSalvo.getId()).isPresent());
	}
}
