package br.edu.fatec.exercicio1labv;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.edu.fatec.exercicio1labv.repositories.ICarroRepositoryTest;
import br.edu.fatec.exercicio1labv.repositories.IClienteRepositoryTest;
import br.edu.fatec.exercicio1labv.repositories.IEnderecoRepositoryTest;
import br.edu.fatec.exercicio1labv.repositories.IVendaRepositoryTest;
import br.edu.fatec.exercicio1labv.repositories.IVendedorRepositoryTest;
import br.edu.fatec.exercicio1labv.services.impl.CarroServiceImplTest;
import br.edu.fatec.exercicio1labv.services.impl.ClienteServiceImplTest;
import br.edu.fatec.exercicio1labv.services.impl.VendaServiceTest;
import br.edu.fatec.exercicio1labv.services.impl.VendedorServiceImplTest;

@RunWith(Suite.class)
@SuiteClasses({ Exercicio1LabvApplicationTests.class,ICarroRepositoryTest.class,IClienteRepositoryTest.class,
				IEnderecoRepositoryTest.class,IVendaRepositoryTest.class,IVendedorRepositoryTest.class, 
				CarroServiceImplTest.class, ClienteServiceImplTest.class, VendaServiceTest.class, VendedorServiceImplTest.class})
public class AllTests {

}
