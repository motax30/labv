package br.edu.fatec.exercicio1labv.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.edu.fatec.exercicio1labv.entities.Carro;
import br.edu.fatec.exercicio1labv.entities.Cliente;
import br.edu.fatec.exercicio1labv.entities.Venda;
import br.edu.fatec.exercicio1labv.entities.Vendedor;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class IVendedorRepositoryTest {

	@Autowired
	private IVendaRepository vendaRepository;
	
	@Autowired
	private ICarroRepository carroRepository;
	
	@Autowired
	private IClienteRepository clienteRepository;
	
	@Autowired
	private IVendedorRepository vendedorRepository;
	
	private Cliente cliente;
	private List<Carro> carros;
	private Vendedor vendedor;

	private Cliente clienteUpdate;

	private Vendedor vendedorUpdate;

	private List<Carro> carros2;
	
	
	@Before
	public void setUp() throws Exception {
		Carro carro1 = new Carro.CarroBuilder().setNome("Fusca").setMarca("Chevrolet").setCor("Amarelo").build();
		Carro carro2 = new Carro.CarroBuilder().setNome("Fit").setMarca("Honda").setCor("Cinza").build();
		carroRepository.save(carro1);
		carroRepository.save(carro2);

		carros = carroRepository.findAll();
		
		carros2 = Arrays.asList(carroRepository.save(new Carro.CarroBuilder().setNome("Sandero").setMarca("Renault").setCor("Vermelho").build()),
				carroRepository.save(new Carro.CarroBuilder().setNome("BMW").setMarca("Mitsubish").setCor("Branco").build()));
		
		cliente = new Cliente.Builder()
				.nome("Adriano Mota")
				.cpf(31083818813L)
				.build();
		clienteUpdate = new Cliente.Builder()
				.nome("Fernando Santos")
				.cpf(888111888822L)
				.build();
		clienteRepository.save(cliente);
		vendedor = new Vendedor("Aurélio");
		vendedorRepository.save(vendedor);
		
		vendedorUpdate = new Vendedor("Aurélio");
		vendedorRepository.save(vendedorUpdate);
	}

	@Test
	public void testSalvar() {
		Venda venda = new Venda(cliente, carros,Calendar.getInstance(), 30000D, vendedor);
		vendaRepository.save(venda);
		assertNotNull(vendaRepository.findById(venda.getId()));
	}

	@Test
	public void testRemover() {
		Venda venda = new Venda(cliente, carros,Calendar.getInstance(), 30000D, vendedor);
		vendaRepository.save(venda);
		vendaRepository.delete(venda);
		assertFalse(vendaRepository.findById(venda.getId()).isPresent());
	}

	@Test
	public void testFindById() {
		Venda venda = new Venda(cliente, carros,Calendar.getInstance(), 30000D, vendedor);
		vendaRepository.save(venda);
		assertTrue(vendaRepository.findById(venda.getId()).isPresent());
	}

	@Test
	public void testFindAll() {
		Venda venda = new Venda(cliente, carros,Calendar.getInstance(), 30000D, vendedor);
		vendaRepository.save(venda);
		Venda venda2 = new Venda(cliente, carros,Calendar.getInstance(), 30000D, vendedor);
		vendaRepository.save(venda2);
		assertEquals(2,vendaRepository.findAll().size());
	}

}
