package br.edu.fatec.exercicio1labv.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.edu.fatec.exercicio1labv.entities.Carro;
import br.edu.fatec.exercicio1labv.entities.Cliente;
import br.edu.fatec.exercicio1labv.entities.Endereco;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class IClienteRepositoryTest {

	private Cliente cliente;
	private List<Carro> carros;
	
	@Autowired
	private IClienteRepository clienteRepository;
		
	private Endereco endereco;
	private Endereco enderecoUpdate;
	
	@Before
	public void setUp() {
		carros = Arrays.asList(new Carro.CarroBuilder().setNome("Onix").build(),
				new Carro.CarroBuilder().setNome("Fusca").setMarca("Chevrolet").setCor("Amarelo").build(),
				new Carro.CarroBuilder().setNome("Fit").setMarca("Honda").setCor("Cinza").build(),
				new Carro.CarroBuilder().setNome("Etios").setMarca("Toiota").setCor("Azul Metálico").build());
		endereco = new Endereco.EnderecoBuilder().rua("Otavio de Moraes")
				.numero("298").bairro("Jardim Americano").cidade("São José dps Campos").build();
		enderecoUpdate = new Endereco.EnderecoBuilder().rua("Carlos de Azevedo")
				.numero("885").bairro("Jardim Aquárius").cidade("São Paulo").build();
		cliente = new Cliente.Builder()
				.nome("Adriano Mota")
				.cpf(31083818813L)
				.carros(carros)
				.build();
	}
	
	@Test
	public void testSalvarClienteComSucesso() {	
		cliente.setEndereco(endereco);
		clienteRepository.save(cliente);
		assertNotNull(clienteRepository.findById(cliente.getId()));
	}
	
	@Test
	public void testRemoverClienteSucesso() {
		Cliente c1 = clienteRepository.save(cliente);
		clienteRepository.delete(c1);
		Optional<Cliente> cliente = clienteRepository.findById(c1.getId());
		assertNotSame(c1, cliente);
	}
	
	@Test
	public void testFindById() {
		Cliente c1 = clienteRepository.save(cliente);
		assertTrue("Deveria retornar um carro cadastrado.",clienteRepository.findById(c1.getId()).isPresent());
	}

	@Test
	public void testFindAll() {
		Cliente c1 = clienteRepository.saveAndFlush(cliente);
		Cliente c2 = new Cliente.Builder()
				.nome("Jorge de Carvalho")
				.cpf(25478578596L)
				.endereco(enderecoUpdate)
				.build();
		clienteRepository.saveAndFlush(c2);
		assertEquals(2,clienteRepository.findAll().size());
	}
	
	@Test
	public void testFindByCpf() {
		Cliente c1 = clienteRepository.saveAndFlush(cliente);
		assertEquals("Deveria ter retornado uma instância de Cliente",c1,clienteRepository.findByCpf(c1.getCpf()).get());
	}
	
	@Test
	public void testBuscarPorNome() {
		cliente.setNome("Henrique da Silva");
		Cliente c1 = clienteRepository.save(cliente);
		assertEquals(c1, clienteRepository.buscarPorNome(c1.getNome()).get(0));
	}
}
