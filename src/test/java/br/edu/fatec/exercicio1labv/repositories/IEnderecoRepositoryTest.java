package br.edu.fatec.exercicio1labv.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.edu.fatec.exercicio1labv.entities.Endereco;
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class IEnderecoRepositoryTest {

	@Autowired
	private IEnderecoRepository enderecoRepository;
	
	@Test
	public void testSave() {
		Endereco endereco = new Endereco.EnderecoBuilder().rua("Otavio de Moraes")
				.numero("298").bairro("Jardim Americano").cidade("São José dps Campos").build();
		enderecoRepository.save(endereco);
		assertTrue(enderecoRepository.findById(endereco.getId()).isPresent());
	}

	@Test
	public void testFindById() {
		Endereco endereco = new Endereco.EnderecoBuilder().rua("Otavio de Moraes")
				.numero("298").bairro("Jardim Americano").cidade("São José dps Campos").build();
		enderecoRepository.save(endereco);
		assertTrue(enderecoRepository.findById(endereco.getId()).isPresent());
	}

	@Test
	public void testFindAll() {
		Endereco endereco = new Endereco.EnderecoBuilder().rua("Otavio de Moraes")
				.numero("298").bairro("Jardim Americano").cidade("São José dps Campos").build();
		Endereco endereco2 = new Endereco.EnderecoBuilder().rua("Otavio de Moraes")
				.numero("298").bairro("Jardim Americano").cidade("São José dps Campos").build();
		enderecoRepository.saveAll(Arrays.asList(endereco,endereco2));
		assertEquals(2, enderecoRepository.findAll().size());
	}

	@Test
	public void testDelete() {
		Endereco endereco = new Endereco.EnderecoBuilder().rua("Otavio de Moraes")
				.numero("298").bairro("Jardim Americano").cidade("São José dps Campos").build();
		enderecoRepository.save(endereco);
		enderecoRepository.delete(endereco);
		assertFalse(enderecoRepository.findById(endereco.getId()).isPresent());
	}

}
